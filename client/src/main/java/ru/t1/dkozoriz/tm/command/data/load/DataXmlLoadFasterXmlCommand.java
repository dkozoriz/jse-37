package ru.t1.dkozoriz.tm.command.data.load;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataJsonLoadJaxBRequest;
import ru.t1.dkozoriz.tm.dto.request.data.load.DataXmlLoadFasterXmlRequest;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    public DataXmlLoadFasterXmlCommand() {
        super("data-load-xml", "load data from xml file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getEndpointLocator().getDomainEndpoint().loadXmlFaster(new DataXmlLoadFasterXmlRequest(getToken()));
    }

}