package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public ProjectCreateCommand() {
        super("project-create", "create new project.");
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        getEndpointLocator().getProjectEndpoint()
                .projectCreate(new ProjectCreateRequest(getToken(), name, description));
    }

}
