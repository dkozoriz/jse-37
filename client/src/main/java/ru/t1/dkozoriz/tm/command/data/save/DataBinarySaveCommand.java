package ru.t1.dkozoriz.tm.command.data.save;

import lombok.SneakyThrows;
import ru.t1.dkozoriz.tm.command.data.AbstractDataCommand;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBase64SaveRequest;
import ru.t1.dkozoriz.tm.dto.request.data.save.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    public DataBinarySaveCommand() {
        super("data-save-bin", "Save data to binary file.");
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        getEndpointLocator().getDomainEndpoint().saveBinary(new DataBinarySaveRequest(getToken()));
    }

}