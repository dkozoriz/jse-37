package ru.t1.dkozoriz.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.ICommand;
import ru.t1.dkozoriz.tm.api.service.IEndpointLocator;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected final String name;

    @Nullable
    protected final String description;

    @Nullable
    protected final String argument;

    protected IServiceLocator serviceLocator;

    protected IEndpointLocator endpointLocator;

    protected AbstractCommand(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        argument = null;
    }

    protected AbstractCommand(@NotNull String name, @Nullable String description, @Nullable String argument) {
        this.name = name;
        this.description = description;
        this.argument = argument;
    }
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    public abstract void execute();

    @NotNull
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}