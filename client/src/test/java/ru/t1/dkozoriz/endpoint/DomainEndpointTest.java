package ru.t1.dkozoriz.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.SoapCategory;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.dto.request.data.load.*;
import ru.t1.dkozoriz.tm.dto.request.data.save.*;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.service.PropertyService;
import ru.t1.dkozoriz.tm.service.TokenService;

@Category(SoapCategory.class)
public class DomainEndpointTest {

    @NotNull
    private static final ITokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IDomainEndpoint DOMAIN_ENDPOINT = IDomainEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @BeforeClass
    public static void setConnection() {
        @NotNull final UserLoginRequest loginRequestAdmin = new UserLoginRequest("admin", "admin");
        @Nullable final String adminToken = AUTH_ENDPOINT.login(loginRequestAdmin).getToken();
        Assert.assertNotNull(adminToken);
        TOKEN_SERVICE.setToken(adminToken);
    }

    @Test
    public void testSaveBackup() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBackup(request));
    }

    @Test
    public void testLoadBackup() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBackup(request));
    }

    @Test
    public void testSaveBase64() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBase64(request));
    }

    @Test
    public void testLoadBase64() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBase64(request));
    }

    @Test
    public void testSaveBinary() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveBinary(request));
    }

    @Test
    public void testLoadBinary() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadBinary(request));
    }

    @Test
    public void testSaveJsonFaster() {
        @NotNull final DataJsonSaveFasterRequest request = new DataJsonSaveFasterRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveJsonFaster(request));
    }

    @Test
    public void testLoadJsonFaster() {
        @NotNull final DataJsonLoadFasterRequest request = new DataJsonLoadFasterRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadJsonFaster(request));
    }

    @Test
    public void testSaveJsonJaxB() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveJsonJaxB(request));
    }

    @Test
    public void testLoadJsonJaxB() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadJsonJaxB(request));
    }

    @Test
    public void testSaveXmlFasterXml() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveXmlFaster(request));
    }

    @Test
    public void testLoadXmlFasterXml() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadXmlFaster(request));
    }

    @Test
    public void testSaveXmlJaxB() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveXmlJaxB(request));
    }

    @Test
    public void testLoadXmlJaxB() {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadXmlJaxB(request));
    }

    @Test
    public void testSaveYaml() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.saveYaml(request));
    }

    @Test
    public void testLoadYaml() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(TOKEN_SERVICE.getToken());
        Assert.assertNotNull(DOMAIN_ENDPOINT.loadYaml(request));
    }

}