package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, connectionService, projectRepository, taskRepository, propertyService);

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setPasswordHash("password " + i);
            user.setEmail("user" + i + "@mail");
            userList.add(user);
            userRepository.add(user);
        }
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User user = new User();
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testAddUserNegative() {
        @Nullable final User user = null;
        Assert.assertThrows(EntityException.class, () -> userService.add(user));
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        userService.clear();
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testFindAllUser() {
        Assert.assertEquals(userList.size(), userService.findAll().size());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = userList.get(0);
        userService.removeOne(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertNull(userService.removeOne(null));

    }

    @Test
    public void testRemoveUserById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final User user = userList.get(0);
        userService.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testRemoveUserByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(id));
    }

    @Test
    public void testRemoveUserByLogin() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final String login = userList.get(0).getLogin();
        userService.removeByLogin(login);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testRemoveUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin("not exist user"));
    }

    @Test
    public void testRemoveUserByEmail() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final String email = userList.get(0).getEmail();
        userService.removeByEmail(email);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testRemoveUserByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByEmail("not exist user"));
    }

    @Test
    public void testFindUserById() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.findById(user.getId()));
            Assert.assertEquals(user, userService.findById(user.getId()));
        }
    }

    @Test
    public void testFindUserByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> userService.findById(id));
    }

    @Test
    public void testGetSizeUser() {
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testSetUser() {
        @NotNull List<User> users = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        users.add(user1);
        users.add(user2);
        final int expectedNumberOfEntries = users.size();
        Assert.assertEquals(expectedNumberOfEntries, userService.set(users).size());
    }

    @Test
    public void testAddUserCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<User> users = new ArrayList<>();
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.add(users);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testCreateEmail() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final String email = "email";
        @NotNull final User user1 = new User();
        user1.setLogin(login);
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        user1.setEmail(email);
        @NotNull final User user2 = userService.create(login, password, email);
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
    }

    @Test
    public void testCreateEmailNegative() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final String email = "email";
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password, email));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", password, email));
        @Nullable final String login2 = userList.get(0).getLogin();
        Assert.assertThrows(LoginIsExistException.class, () -> userService.create(login2, password, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, null, email));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, "", email));
        @Nullable final String email2 = userList.get(0).getEmail();
        Assert.assertThrows(EmailIsExistException.class, () -> userService.create(login, password, email2));
    }

    @Test
    public void testCreateNegative() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", password));
        @Nullable final String login2 = userList.get(0).getLogin();
        Assert.assertThrows(LoginIsExistException.class, () -> userService.create(login2, password));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, ""));
        @Nullable final String email2 = userList.get(0).getEmail();
        Assert.assertThrows(EmailIsExistException.class, () -> userService.create(login, password, email2));
    }

    @Test
    public void testCreateRole() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final Role role = Role.USUAL;
        @NotNull final User user1 = new User();
        user1.setLogin(login);
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        user1.setRole(role);
        @NotNull final User user2 = userService.create(login, password, role);
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getRole(), user2.getRole());
    }

    @Test
    public void testCreateRoleNegative() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final Role role = Role.USUAL;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password, role));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", password, role));
        @Nullable final String login3 = userList.get(0).getLogin();
        Assert.assertThrows(LoginIsExistException.class, () -> userService.create(login3, password, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, null, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, "", role));
        @Nullable final Role role2 = null;
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(login, password, role2));
    }

    @Test
    public void testIsLoginExist() {
        @NotNull final String login = userList.get(0).getLogin();
        @NotNull final Boolean loginExist1 = userList.stream().anyMatch(u -> login.equals(u.getLogin()));
        @NotNull final Boolean loginExist2 = userService.isLoginExist(login);
        Assert.assertEquals(loginExist1, loginExist2);
        Assert.assertFalse(userService.isLoginExist(null));
        Assert.assertFalse(userService.isLoginExist(""));
    }

    @Test
    public void testIsEmailExist() {
        @NotNull final String email = userList.get(0).getEmail();
        @NotNull final Boolean emailExist1 = userList.stream().anyMatch(u -> email.equals(u.getEmail()));
        @NotNull final Boolean emailExist2 = userService.isEmailExist(email);
        Assert.assertEquals(emailExist1, emailExist2);
        Assert.assertFalse(userService.isEmailExist(null));
        Assert.assertFalse(userService.isEmailExist(""));
    }

    @Test
    public void testFindUserByLogin() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
    }

    @Test
    public void testFindUserByEmail() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindUserByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
    }

    @Test
    public void testSetPassword() {
        @NotNull final User user1 = userList.get(0);
        @NotNull final String password = "password";
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final User user2 = userList.get(0);
        Assert.assertEquals(user1.getPasswordHash(), userService.setPassword(user2.getId(), password).getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        @NotNull final User user1 = userList.get(0);
        @NotNull final String password = "password";
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, password));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword("", password));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user1.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user1.getId(), ""));
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String userId = userList.get(0).getId();
        @NotNull final String name = "test";
        userList.get(0).setFirstName(name);
        userList.get(0).setMiddleName(name);
        userList.get(0).setLastName(name);
        @NotNull User user = userService.updateUser(userId, name, name, name);
        Assert.assertEquals(userList.get(0).getFirstName(), user.getFirstName());
        Assert.assertEquals(userList.get(0).getMiddleName(), user.getMiddleName());
        Assert.assertEquals(userList.get(0).getLastName(), user.getLastName());
    }

    @Test
    public void testUpdateUserNegative() {
        @NotNull final String name = "test";
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(null, name, name, name));
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser("", name, name, name));
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final User user = userList.get(0);
        @NotNull final String login = user.getLogin();
        user.setLocked(true);
        userService.lockUserByLogin(login);
        Assert.assertEquals(user.getLocked(), userService.findByLogin(login).getLocked());
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(EntityException.class, () -> userService.lockUserByLogin("not exist user"));
    }

    @Test
    public void testUnLockUserByLogin() {
        @NotNull final User user = userList.get(0);
        @NotNull final String login = user.getLogin();
        user.setLocked(false);
        userService.unLockUserByLogin(login);
        Assert.assertEquals(user.getLocked(), userService.findByLogin(login).getLocked());
    }

    @Test
    public void testUnLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unLockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unLockUserByLogin(""));
        Assert.assertThrows(EntityException.class, () -> userService.unLockUserByLogin("not exist user"));
    }

}