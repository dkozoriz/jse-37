package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.service.business.TaskService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(UnitCategory.class)
public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private final List<Task> taskList = new ArrayList<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Before
    public void initRepository() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId(PROJECT_ID_1);
            }
            else {
                task.setUserId(USER_ID_2);
                task.setProjectId(PROJECT_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testAddTaskNegative() {
        @Nullable final Task task = null;
        Assert.assertThrows(EntityException.class, () -> taskService.add(task));
    }

    @Test
    public void testClearTask() {
        final int expectedNumberOfEntries = 0;
        taskService.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userTaskList.size();
        taskService.clear(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testClearUserOwnedTaskNegative() {
        @Nullable final String userId1 = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(userId1));
        @Nullable final String userId2 = "";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(userId2));
    }

    @Test
    public void testFindAllTask() {
        Assert.assertEquals(taskList.size(), taskService.findAll().size());
    }

    @Test
    public void testFindAllUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userTaskList.size();
        Assert.assertEquals(expectedNumberOfEntries, taskService.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllTaskSort() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = userTaskList.size();
        final Sort sort = Sort.toSort("BY_NAME");
        Assert.assertEquals(expectedNumberOfEntries, taskService.findAll(USER_ID_1, sort).size());
    }

    @Test
    public void testFindAllUserOwnedTaskNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(userId));
    }

    @Test
    public void testFindAllTaskSortNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(userId, Sort.toSort("BY_NAME")));
    }

    @Test
    public void testRemoveTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskService.remove(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveTaskNegative() {
        @Nullable final Task task = null;
        Assert.assertThrows(EntityException.class, () -> taskService.remove(task));
    }

    @Test
    public void testRemoveUserOwnedTask() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskRepository.remove(USER_ID_1, task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveUserOwnedTaskNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(userId, taskList.get(0)));
        @Nullable final Task task = null;
        Assert.assertThrows(EntityException.class, () -> taskService.remove(USER_ID_1, task));
    }

    @Test
    public void testRemoveTaskById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskService.removeById(task.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveTaskByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(id));
        @Nullable final String id2 = "";
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(id2));
    }

    @Test
    public void testRemoveUserOwnedTaskById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final Task task = taskList.get(0);
        taskService.removeById(USER_ID_1, task.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveUserOwnedTaskByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(userId, taskId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, id));
    }

    @Test
    public void testRemoveUserOwnedTaskByIndex() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final int index = 0;
        taskService.removeByIndex(USER_ID_1, index);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testRemoveUserOwnedTaskByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeByIndex(userId, 0));
        @Nullable final Integer index = -1;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.removeByIndex(USER_ID_1, index));
    }


    @Test
    public void testFindTaskById() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskService.findById(task.getId()));
            Assert.assertEquals(task, taskService.findById(task.getId()));
        }
    }

    @Test
    public void testFindUserOwnedTaskById() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : userTaskList) {
            Assert.assertNotNull(taskService.findById(USER_ID_1, task.getId()));
            Assert.assertEquals(task, taskService.findById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindUserOwnedTaskByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(userId, taskId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(USER_ID_1, id));
    }

    @Test
    public void testFindTaskByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findById(id));
    }

    @Test
    public void testFindUserOwnedTaskByIndex() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userTaskList.size(); i++) {
            Assert.assertNotNull(taskService.findByIndex(USER_ID_1, i));
            Assert.assertEquals(taskList.get(i), taskService.findByIndex(USER_ID_1, i));
        }
    }

    @Test
    public void testFindUserOwnedTaskByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findByIndex(userId, 0));
        @Nullable final Integer index = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.findByIndex(USER_ID_1, index));
    }

    @Test
    public void testGetSizeTask() {
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedTask() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(userTaskList.size(), taskService.getSize(USER_ID_1));
    }

    @Test
    public void testSetTask() {
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        final int expectedNumberOfEntries = tasks.size();
        Assert.assertEquals(expectedNumberOfEntries, taskService.set(tasks).size());
    }

    @Test
    public void testAddTaskCollection() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 2;
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.add(tasks);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    public void testExistTaskById() {
        @NotNull final List<Task> userTaskList = taskList
                .stream()
                .filter(p -> USER_ID_1.equals(p.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : userTaskList) {
            Assert.assertTrue(taskService.existsById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testExistTaskByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.existsById(userId, taskId));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService.existsById(USER_ID_1, id));
    }

    @Test
    public void testChangeStatusById() {
        @Nullable final Task task = taskService
                .changeStatusById(USER_ID_1, taskList.get(0).getId(), Status.IN_PROGRESS);
         @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .changeStatusById(userId, taskId, Status.IN_PROGRESS));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService
                .changeStatusById(USER_ID_1, id, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> taskService
                .changeStatusById(USER_ID_1, taskId, status));
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable final Task task = taskService
                .changeStatusByIndex(USER_ID_1, 0, Status.IN_PROGRESS);
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIndexNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .changeStatusByIndex(userId, 0, Status.IN_PROGRESS));
        @Nullable final Integer index = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService
                .changeStatusByIndex(USER_ID_1, index, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> taskService
                .changeStatusByIndex(USER_ID_1, 0, status));
    }

    @Test
    public void testUpdateById() {
        @Nullable final Task task = taskList.get(0);
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, taskService.updateById(USER_ID_1, task.getId(), name, description).getName());
        Assert.assertEquals(description, taskService.updateById(USER_ID_1, task.getId(), name, description).getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String taskId = taskList.get(0).getId();
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .updateById(userId, taskId, name, description));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> taskService
                .updateById(USER_ID_1, id, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .updateById(USER_ID_1, taskId, name2, description));
    }

    @Test
    public void testUpdateByIndex() {
        @Nullable final Integer index = 0;
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, taskService.updateByIndex(USER_ID_1, index, name, description).getName());
        Assert.assertEquals(description, taskService.updateByIndex(USER_ID_1, index, name, description).getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        @Nullable final String userId = null;
        @Nullable Integer index = 0;
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .updateByIndex(userId, index, name, description));
        @Nullable final Integer index2 = null;
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService
                .updateByIndex(USER_ID_1, index2, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .updateByIndex(USER_ID_1, index, name2, description));
    }

    @Test
    public void testCreateTask() {
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        @Nullable final Task task = new Task(name);
        task.setDescription(description);
        Assert.assertEquals(task.getName(), taskService.create(USER_ID_1, name, description).getName());
        Assert.assertEquals(task.getDescription(), taskService.create(USER_ID_1, name, description).getDescription());
    }

    @Test
    public void testCreateTaskNegative() {
        @Nullable final String userId = null;
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .create(userId, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .create(USER_ID_1, name2, description));
        @Nullable final String description2 = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService
                .create(USER_ID_1, name, description2));
    }

    @Test
    public void testCreateTaskProjectId() {
        @Nullable final String projectId = UUID.randomUUID().toString();
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        @Nullable final Task task1 = new Task(name);
        task1.setDescription(description);
        task1.setProjectId(projectId);
        @Nullable final Task task2 = taskService.create(USER_ID_1, name, description, projectId);
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getName(), task2.getName());
    }

    @Test
    public void testCreateTaskProjectIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = UUID.randomUUID().toString();
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .create(userId, name, description, projectId));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> taskService
                .create(USER_ID_1, name2, description, projectId));
        @Nullable final String description2 = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService
                .create(USER_ID_1, name, description2, projectId));
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> projectTaskList = taskList
                .stream()
                .filter(p -> PROJECT_ID_1.equals(p.getProjectId()))
                .collect(Collectors.toList());
        final int expectedNumberOfEntries = projectTaskList.size();
        Assert.assertEquals(expectedNumberOfEntries, taskService.findAllByProjectId(USER_ID_1, PROJECT_ID_1).size());
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        @Nullable final String userId = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService
                .findAllByProjectId(userId, PROJECT_ID_1));
    }

}