package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @Override
    public @NotNull User fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        return user;
    }

    @Override
    @NotNull
    public User add(@NotNull User user) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_LOGIN,
                DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_EMAIL, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setBoolean(2, user.getLocked());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getPasswordHash());
            statement.setString(8, user.getEmail());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    public void update(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, user.getLocked());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getMiddleName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getPasswordHash());
            statement.setString(6, user.getRole().toString());
            statement.setString(7, user.getId());
            statement.executeUpdate();
        }
    }

    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), DBConstants.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    public User findByEmail(@Nullable final String email) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), DBConstants.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    public boolean isLoginExist(@NotNull final String login) throws Exception {
        return findAll()
                .stream()
                .anyMatch(u -> login.equals(u.getLogin()));
    }

    public boolean isEmailExist(@NotNull final String email) throws Exception {
        return findAll()
                .stream()
                .anyMatch(u -> email.equals(u.getEmail()));
    }

}