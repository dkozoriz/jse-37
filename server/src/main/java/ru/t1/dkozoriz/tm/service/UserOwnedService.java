package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IUserOwnedService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.sql.Connection;
import java.util.List;

public abstract class UserOwnedService<T extends UserOwnedModel, R extends IUserOwnedRepository<T>>
        extends AbstractService<T, R> implements IUserOwnedService<T> {

    public UserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedRepository<T> getRepository(@NotNull final Connection connection);

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<T> repository = getRepository(connection);
        try {
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }

        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<T> repository = getRepository(connection);
            return repository.existById(userId, id);
        }
    }

    @NotNull
    @Override
    public List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<T> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public T findById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<T> repository = getRepository(connection);
            return repository.findById(userId, id);
        }
    }

    @Nullable
    @Override
    public T findByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<T> repository = getRepository(connection);
            @Nullable final T model = repository.findByIndex(userId, index);
            if (model == null) throw new EntityException(getName());
            return model;
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<T> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    public T removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable T model = findById(userId, id);
        return remove(userId, model);
    }

    @Nullable
    @Override
    public T removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable T model = findByIndex(userId, index);
        return remove(userId, model);
    }

    @Nullable
    public T remove(@Nullable final String userId, @Nullable final T model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityException(getName());
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<T> repository = getRepository(connection);
        try {
            repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

}