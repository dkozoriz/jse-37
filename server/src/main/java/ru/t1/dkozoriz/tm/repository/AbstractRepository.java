package ru.t1.dkozoriz.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.comparator.CreatedComparator;
import ru.t1.dkozoriz.tm.comparator.StatusComparator;
import ru.t1.dkozoriz.tm.model.AbstractModel;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public abstract class AbstractRepository<T extends AbstractModel> implements IAbstractRepository<T> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    public abstract T fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract T add(@NotNull final T model) throws Exception;

    @NotNull
    public abstract void update(@NotNull final T model) throws Exception;

    @Override
    public void clear() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    @NotNull
    public List<T> findAll() throws Exception {
        @NotNull final List<T> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    public T findById(@NotNull final String id) throws Exception {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    public T findByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) resultSet.next();
            return fetch(resultSet);
        }
    }

    public int getSize() throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    public void remove(@Nullable final T model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstants.COLUMN_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
    }

    public void removeAll(@NotNull final Collection<T> collection) throws Exception {
        for (@NotNull final T model : collection) remove(model);
    }

    @NotNull
    public Collection<T> add(@NotNull Collection<T> models) throws Exception {
        @NotNull final List<T> result = new ArrayList<T>();
        for (T model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    public Collection<T> set(@NotNull Collection<T> models) throws Exception {
        clear();
        return add(models);
    }

}