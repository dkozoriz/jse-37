package ru.t1.dkozoriz.tm.api.service;

import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;

public interface IServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    IDomainService getDomainService();

    ISessionService getSessionService();

}