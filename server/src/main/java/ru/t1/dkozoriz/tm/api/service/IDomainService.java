package ru.t1.dkozoriz.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    Domain getDomain() throws Exception;

    void setDomain(@Nullable Domain domain) throws Exception;

    @SneakyThrows
    void dataBackupLoad();

    @SneakyThrows
    void dataBackupSave();

    @SneakyThrows
    void dataBase64Load();

    @SneakyThrows
    void dataBase64Save();

    @SneakyThrows
    void dataBinaryLoad();

    @SneakyThrows
    void dataBinarySave();

    @SneakyThrows
    void dataJsonLoadFaster();

    @SneakyThrows
    void dataJsonSaveFaster();

    @SneakyThrows
    void dataJsonLoadJaxB();

    @SneakyThrows
    void dataJsonSaveJaxB();

    @SneakyThrows
    void dataXmlLoadFasterXml();

    @SneakyThrows
    void dataXmlSaveFasterXml();

    @SneakyThrows
    void dataXmlLoadJaxB();

    @SneakyThrows
    void dataXmlSaveJaxB();

    @SneakyThrows
    void dataYamlLoad();

    @SneakyThrows
    void dataYamlSave();

}