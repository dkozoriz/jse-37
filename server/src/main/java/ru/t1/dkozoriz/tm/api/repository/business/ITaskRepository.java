package ru.t1.dkozoriz.tm.api.repository.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @NotNull String projectId) throws Exception;

}