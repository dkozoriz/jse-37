package ru.t1.dkozoriz.tm.repository.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.util.DBConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends BusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @NotNull
    @Override
    public Task fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setStatus(Status.valueOf(row.getString(DBConstants.COLUMN_STATUS)));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_USER_ID,
                DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_CREATED
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, task.getStatus().toString());
            statement.setTimestamp(7, new Timestamp(task.getCreated().getTime()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) throws Exception {
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public void update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getStatus().toString());
            statement.setString(5, task.getProjectId());
            statement.setString(6, task.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

}