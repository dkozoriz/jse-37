package ru.t1.dkozoriz.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;
import ru.t1.dkozoriz.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dkozoriz.tm.dto.response.system.ServerAboutResponse;
import ru.t1.dkozoriz.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        response.setApplicationName(propertyService.getApplicationName());
        response.setBranch(propertyService.getGitBranch());
        response.setMessage(propertyService.getGitCommitMessage());
        response.setCommitId(propertyService.getGitCommitId());
        response.setTime(propertyService.getGitCommitTime());
        response.setCommitterName(propertyService.getGitCommitterName());
        response.setCommitterEmail(propertyService.getGitCommitterEmail());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}