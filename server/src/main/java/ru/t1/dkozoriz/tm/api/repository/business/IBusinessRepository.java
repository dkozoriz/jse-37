package ru.t1.dkozoriz.tm.api.repository.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<T extends BusinessModel> extends IUserOwnedRepository<T> {

    @Nullable
    List<T> findAll(@Nullable String userId, @NotNull Comparator<? super IWBS> comparator) throws Exception;

}