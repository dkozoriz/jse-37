package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IAbstractRepository<T> {

    @NotNull
    List<T> findAll() throws Exception;

    void clear() throws Exception;

    @Nullable
    T findById(@NotNull String id) throws Exception;

    int getSize() throws Exception;

    void remove(@Nullable T project) throws Exception;

    @NotNull
    Collection<T> add(@NotNull Collection<T> models) throws Exception;

    @NotNull
    Collection<T> set(@NotNull Collection<T> models) throws Exception;

    @NotNull
    T add(@NotNull T model) throws Exception;

    void update(@NotNull final T model) throws Exception;

}