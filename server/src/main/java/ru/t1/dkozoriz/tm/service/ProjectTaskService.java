package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IProjectTaskService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;

import java.sql.Connection;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final IServiceLocator serviceLocator;

    private final static String TASK = "task";

    private final static String PROJECT = "project";

    public ProjectTaskService(@NotNull final IConnectionService connectionService, @NotNull IServiceLocator serviceLocator) {
        this.connectionService = connectionService;
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = serviceLocator.getProjectService().findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        @Nullable final Task task = serviceLocator.getTaskService().findById(taskId);
        if (task == null) throw new EntityException(TASK);
        task.setProjectId(projectId);
        serviceLocator.getTaskService().update(task);
        return task;
    }

    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project = serviceLocator.getProjectService().findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAllByProjectId(userId, projectId);
        for (final Task task : tasks) serviceLocator.getTaskService().removeById(task.getId());
        serviceLocator.getProjectService().removeById(projectId);
    }

    @NotNull
    public Task unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = serviceLocator.getProjectService().findById(projectId);
        if (project == null) throw new EntityException(PROJECT);
        @Nullable final Task task = serviceLocator.getTaskService().findById(taskId);
        if (task == null) throw new EntityException(TASK);
        task.setProjectId(null);
        serviceLocator.getTaskService().update(task);
        return task;
    }

}