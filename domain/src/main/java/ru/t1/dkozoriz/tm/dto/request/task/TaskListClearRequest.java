package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListClearRequest extends AbstractUserRequest {

    public TaskListClearRequest(@Nullable final String token) {
        super(token);
    }

}