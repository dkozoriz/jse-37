package ru.t1.dkozoriz.tm.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResultResponse;
import ru.t1.dkozoriz.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public LoginUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}